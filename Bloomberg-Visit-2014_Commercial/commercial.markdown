% Benefits of Software Freedom for Commercial Activity
% Bradley M. Kuhn
% Tuesday 15 April 2014

# Who Am I?

+ President &amp; Distinguished Technologist of Software Freedom Conservancy.

+ A 501(c)(3) not-for-profit charity dedicated to promoting, advancing, defending, and developing Free, Libre and Open Source Software.

+ I'm not an analyst, historian, nor a researcher.

+ I do software freedom politics, advocacy and non-profit management.

+ My comments are from that perspective.


# What Is This Stuff?

+ You've probably heard about Open Source.

+ Software freedom is the more political way to talk about it.
     + Think in terms of: what rights and freedoms you get with the software?

+ This is a philosophical moral position that's developed over decades.

+ It's not incompatible with capitalism
     + (in fact, it fits right in there).
     + More on that later.

# Ye Ol' Four Freedoms

+ to use.

+ to learn and modify for yourself.

+ to copy and share.

+ to modify and share modified versions.

# Philosophical Underpinnings

<img src="rms-80s-scaled.png" align="right" />

+ 1984-09-27: GNU Manifesto.

+ Richard M. Stallman (RMS) wrote in that *GNU Manifesto*:
      - *"All sorts of development can be funded with a Software Tax"*
      - *"People with new ideas could distribute programs as [free software], asking for donations from satisfied users"*

+ Purely non-profit fundraising style was initial funding idea.

# Initial Development

+ Most early Free Software was written by academics (or those affiliated with
  academic institutions).

+ Minimal initial commercial interest.

+ Meanwhile &hellip;

# Rebutted Objections

+ Obviously, people said RMS was a communist.
      - sharing all your software does have a certain hippie spirit to it.

+ RMS begins to add a Rebutted Objections section:
      - beginnings of the business models debate.

<hr/>
<span class="fitonslide">
<p><strong>&ldquo;Won't programmers starve?&rdquo;</strong></p>
<p>I could answer that nobody is forced to be a programmer. Most of us cannot manage to get any money for standing on the street and making faces. But we are not, as a result, condemned to spend our lives standing on the street making faces, and starving. We do something else.</p>
<p>But that is the wrong answer because it accepts the questioner's implicit
assumption: that without ownership of software, programmers cannot possibly
be paid a cent. Supposedly it is all or nothing.</p>
<p align=right>
 &mdash; Richard M. Stallman, *GNU Manifesto* (updated in 1985)
</p>
</span>

# Justify Your Existence

+ Most social justice causes have to make their case to corporate business.

+ Consider environmentalism:
      + &ldquo;if I recycle, and my competitor doesn't, I'm at a disadvantage.&rdquo;
      + So, how do we get to recycling bins everywhere?

+ Open, oft-debated question: is this kowtowing or good advocacy strategy?

# The Classic Business Model

+ Early idealists centered around two specific business models:
      + Sell support contracts.
      + Sell custom modifications.
      + These showed early success.

+ Capitalism works best:
      + In a competitive free market.
      + With no barriers to entry.
      + Free Software codebases provide that market.
      
# The Cygnus Era

+ The smartest move Red Hat ever made was acquiring Cygnus with its IPO
  capital.

+ Cygnus built the first successful software freedom business.

+ How and why did this happen?

# The First Essential Codebase

+ GNU Compiler Collection (GCC):
       + first Free Software program no one could live without.

+ It replaced a fragmented proprietary market with weak products:
       + with a single well designed product.
       + under a copyleft license (more on that later).

+ Cygnus' founders realized this early enough to act.
       + Rather than first to market, it's more &ldquo;first to talent&rdquo;.

# Why GCC Needed Commerce

+ Chipset manufacturers needed compilers for their new architectures quickly.

+ Free Software projects always move slow.

+ Not easy to sign a deadline-centered deal with a non-profit.

+ Infrastructure and motivations of a commercial actor is essential.

# What Stopped Proprietarization?

+ The real innovation isn't the philosophical realization of four freedoms.

+ Copyleft guarantees a truly free market.

+ Proprietarized Free Software can create new monopolies.
      + e.g., Apple OSX.

# So, What's Copyleft?

+ You may have heard this part before, but hopefully it's worth repeating.

+ Briefly, a first principles explanation of software freedom licensing.

# Why Do We Need Licenses?

+ None of those freedoms happen by default.

+ Software is governed by copyright &hellip;
       + &hellip; since it's expression fixed in a tangible medium.

       + &ldquo;To promote the Progress of Science and useful Arts, by
       securing for limited Times to Authors and Inventors the exclusive
       Right to their respective Writings and Discoveries;&rdquo;
       
+ only way to give those freedoms is to give a license.

# What Free Software License Does?

+ Copyright on software governs three basic activities:
      + copying, modifying and distributing.

+ By default, you're prohibited from doing those things.

+ A license is:
      + a document that gives you permissions &hellip;
      + &hellip; that you otherwise wouldn't have.

+ Free software license, first and foremost:
      + assert recipient's to copy, share, modify and redistribute the software.

# Does/Should a License Do More?

+ Ultimately depends on the authors' goal.
      + More on this in a moment.

+ Every Free Software license gives first recipient those four freedoms.

+ Question is: What about everyone else?

# Basics of Copyleft Licensing

+ Concept: share and share alike.

+ Accomplished via copyright requirements:
      + distribution accompanied with complete, corresponding source.
      + distribution of modified versions need same (with changes).

+ Generally triggered on distribution:
      + (Note: Affero GPL triggers on modification.)

# Comparing Licenses

<br/>
<br/>

<img src="license-spectrum.png" align="center" />

# Contribution Models

+ Both non-copyleft and copyleft communities **want** changes back.

+ All of them want commercial and non-commercial activity treated equally.

+ Difference is purely on what tactics to use to ensure equality for all.

+ Full agreement on commercial/non-commercial equality.

# Linux: Non-Commercial Licensing Fail &hellip;
<span class="fitonslide">
<p>This kernel is (C) 1991 Linus Torvalds, but all or part of it may be
redistributed provided you do the following:</p>
<p>&hellip;
<ul>
<li>You may not distibute [sic] this for a fee, not even "handling"
	  costs.</li>
</p>
<p>Mail me at "torvalds@kruuna.helsinki.fi" if you have any questions.</p>
</span>


# &hellip; Within Six Months
<span class="fitonslide">
<p>I've had a couple of requests to make
it compatible with the GNU copyleft, removing the "you may not
distribute it for money" condition.  I agree.  I propose that the
copyright be changed so that it confirms to GNU - pending approval of
the persons who have helped write code.  I assume this is going to be no
problem for anybody: If you have grievances ("I wrote that code assuming
the copyright would stay the same") mail me.  Otherwise The GNU copyleft
takes effect as of the first of February [1992]. </p>

# The Advent of Open Source

+ By the turn of the century:
     + <a
       href="http://web.archive.org/web/19990430060825/http://www.gnu.org/philosophy/free-sw.html">FSF's
       Free Software Definition</a> adds various text about importance of
       commercial freedoms that <a
       href="http://web.archive.org/web/19990117030322/http://www.gnu.org/philosophy/free-sw.html">did
       not appear previously</a> in the definition.

     + DFSG and Open Source Definition are published with &ldquo;No
       Discrimination Against Fields of Endeavor&rdquo; clause.

+ No question of commercial adoption or interest.

# How To Interact?

+ Formation of Apache Software Foundation.

+ IBM's famous statement regarding Apache:
      + &ldquo;How do we make a deal with a website?&rdquo;

+ Became the non-copyleft center hub, in the way FSF was the copyleft center
  hub.

+ Why do it this way?

# For/Non-profit Interaction

+ Free Software projects are run by individuals.

+ Most such individuals are employed.
      + these days, often because of their expertise and leadership in specific projects.

+ Their collaborators are at their boss' competitors.

+ Individual contributors need a &ldquo;gathering institution&rdquo;

+ Enter: non-profits.

# For-Profit vs. Non-Profit

+ For-profits act in interest in shareholders.

+ 501(c)(3) non-profits act in interest of the public good.

+ Software freedom is best when in public good.

+ Non-profit orgs (NPOs) are best place for software freedom.

# For-Profit-Employee &ldquo;Volunteers&rdquo;

+ Most code written by for-profit employees.
     + *20% time* is a boon to software freedom.
     + **But** codebases drift in directions of company's needs.

+ Some developers should be *funded* by non-profits&hellip;
     - to mitigate dangers of for-profit control.

<hr/>

> It's the duty of all Free Software developers to steal as much time as they can from their employers for software freedom.

<span class="fitonslide">
<p align=right>
 &mdash; Jeremy Allison, Director, Conservancy &amp; Co-Founder, Samba Project
</p>
</span>

# Historical Public Good Models

+ FSF, Apache Software Foundation, Software in the Public Interest, Open
  Source Initiative are all 501(c)(3)'s.

+ They **must** act in the public interest, legally &hellip;
       + &hellip; lest they lose their tax-exempt status.

+ Businesses may not control them:
       + private benefit is strictly prohibited.

# The Rise of For-Profit Open Source

+ I'll be the first to tell you:
       + open source is a business fad.

+ Open sourcing your stuff does not:
       + magically make your code better.
       + get unpaid &ldquo;community&rdquo; labor to improve your product.
       + MAKE.MONEY.FAST (VA Linux is out, Candy Crush is in)

+ Yet, business-centric projects don't have community volunteers.

# The March of the Trade Association

+ 501(c)(6)'s are a different form of non-profit.

+ They act in the common business interest of all their members.
       + most of which are for-profit companies.

+ An effective model for business-centric software.

+ Community of individuals does **not** come first.

+ Orgs like OpenStack Foundation and Linux Foundation seem to be growing too
  fast for their own good.

+ The model admittedly has value.

# Copyleft Under Attack

+ Many (particularly in (c)(6)'s) now argue copyleft is no longer needed.

+ Most heavily funded large projects under non-copyleft are young.
       + e.g, Open Stack, LLVM.

+ There's still no adequate &rdquo;tragedy of the commons&rdquo; answer for
  non-copyleft.

+ The data here is so bad, and situations unprecedented, we just have to wait
  and see.

+ You can probably guess my opinions.

# Software Freedom For The Developer

+ Regardless of license, let employees contribute!

+ Most of the best software innovation won't happen here.
       + Even if you're Google, that can't last forever.
       
+ Employee brain-drain is a real social force.

+ Empowering employees to participate in their community makes happy
  employees.

+ Mailing lists and shared patches *are* the community of software
  developers.

+ Most software you use isn't your core product anyway.

# Software Freedom For Entrepreneurs

+ Various proprietary software cartels.
       - PBX is an excellent example.

+ Free software work is an opportunity to break these up.

+ Current places this is occurring:
       + Medical records: OpenMRS
       + GIS: QGIS displacing ArcView

+ Places it could occur:
       + CAD: Autodesk
       + Non-Profit Accounting Systems: Blackbaud

# Why We Need Commercial Interest

+ Non-profit funding is a necessary component.

+ For-profits always have more resources.
      + Great Arts always subsidized by the most wealthy.
      + And, in our society, that's big corporations.

+ Counterbalance of equality: software freedom licensing.

<hr/>
> I haven't finished doing the math, but I'm pretty sure we can't *all* make a living hustling each other for money on Kickstarter.

<span class="fitonslide">
<p align=right>
 &mdash; Evan Prodromou, Free Software Entrepreneur and Founder, E14N
</p>
</span>

# What is Conservancy?

+ Conservancy is a 501(c)(3) non-profit home for Free Software projects.

+ Individual developers' projects can join; we are the non-profit home.

+ Conservancy serves the needs of the project and the public good.
       + but developers retain artistic control.

+ Build governance outside of for-profit control, but permit contribution as individuals.

# Conservancy Member Projects

<img src="Conservancy-project-logos-NASCAR-page-2014-02-05.png" />

# More Info / Talk License

+ URLs / Social Networking / Email:
     - A Book I helped write: <a
        href="http://ebb.org/bkuhn/writings/comprehensive-gpl-guide.pdf">*Copyleft and
        the GNU General Public License: A Comprehensive Tutorial*</a> is available.
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: faif.us &amp; ebb.org/bkuhn
     - Slides: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)
     - DONATE: https://sfconservancy.org/donate/

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2011, 2012, 2013, 2014 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>
<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
