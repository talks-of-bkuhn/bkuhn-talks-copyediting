% Non-Profit Infrastructure for Software Freedom
% Bradley M. Kuhn
% Sunday 21 October 2012

# Who Am I?

+ Two official roles:
    - On Board of Directors of Free Software Foundation.
    - President and Executive Director of Software Freedom Conservancy.

+ Many of my views are influenced by these roles.

+ These are still my views, not those of these orgs or others.

# The Rise of the Volunteers

+ Most Free Software is written by volunteers &hellip;
     + &hellip; who are only such from the point of view of the projects.

+ The volunteers are usually paid for by for-profits
     - Red Hat, Google, IBM, etc.

# For-Profit vs. Non-Profit

+ For-profits act in interest in shareholders.

+ 501(c)(3) non-profits act in interest of the public good.

+ Software freedom is best when in public good.

+ Non-profit orgs (NPOs) are best place for software freedom.

+ NPOs can accept for-profit donations, but provide a firewall.

# For-Profit-Employee &ldquo;Volunteers&rdquo;

+ Most code written by for-profit employees.
     + *20% time* is a boon to software freedom.
     + **But** codebases drift in directions of company's needs.

+ Some developers should be *funded* by non-profits,
     - to mitigate dangers of for-profit control.

***

> It's the duty of all Free Software developers to steal as much time as they can from their employers for software freedom.

<span class="fitonslide">
<p align=right>
 &mdash; Jeremy Allison, Director, Conservancy &amp; Co-Founder, Samba Project
</p>
</span>

# What Can 501(c)(3)'s Do?

+ Collect (USA-tax-deductible) donations for a project.
      - both individual and corporate.

+ Distribute that money to advance project (and public good).

+ Make sure project isn't controlled by for-profit interests.

+ Help leadership with non-technical decisions.

# What Can 501(c)(6)'s do?

+ Donations not tax-deductible.

+ Controlled by a group of companies.

+ Acts in the shared business interest of those companies.

+ Fundraising is often easier, since companies can control projects.

# Organization Proliferation

+ Too many NPOs will confuse donors.

+ Each needs board of directors, incorporation, meetings, IRS filings.

+ Hackers end up doing work they hate.

+ Less Free Software gets written.

+ Rob will talk about how this is not so bad.

# Aggregating Under Umbrella

+ Sometimes called a &ldquo;fiscal sponsor&rdquo;
    + it doesn't mean they give you money &hellip;
    + &hellip; but rather handle non-profit work for you.

+ Current 501(c)(3) options are:
     - Software Freedom Conservancy
     - Software in the Public Interest
     - Free Software Foundation 
     - Apache Software Foundation

+ There are some 501(c)(6) options.
     - Outercurve Foundation
     - Linux Foundation

# Software Freedom Conservancy

+ Requirements:
    - Any license on both FSF's and OSI's lists.
    - Established project.
    - Evaluation by Conservancy's Evaluation Committee.

+ Key benefits:
    - earmarked donations.
    - basic developer liability protection.
    - legal services (including license enforcement)
    - holding assets.
    - conference organizing assistance.
    - fundraising assistance.

# Software in the Public Interest

+ Requirements:
    - License meets DFSG.
    - Evaluation by Board of Directors.

+ Key benefits:
    - earmarked donations.
    - holding assets.

# Free Software Foundation

+ Requirements:
    - Must be part of the GNU Project.

+ Key benefits:
    - earmarked donations.
    - holding assets.

# Apache Software Foundation

+ Requirements:
    - Apache License.
    - Must follow &ldquo;Apache Way&rdquo;.
    - Incubator process.

+ Benefits:
    - Can apply for funding through Apache
        - (but no earmarked donations)
    - Technological infrastructure &amp; hosting.
    - basic developer liability protection.

# Outercurve Foundation

# Going Your Own Way

+ Should only do this if you want to be a non-profit administrator
     + at least some of the time.

# More Info and This Talk's License

+ More Info:
    + Conservancy: http://sfconservancy.org
    + SPI: http://spi-inc.org/
    + Apache: http://apache.org/
    + Outercurve: http://www.outercurve.org/
    + MetaBrainz: http://metabrainz.org/

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2010, 2011, 2012 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>

<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
