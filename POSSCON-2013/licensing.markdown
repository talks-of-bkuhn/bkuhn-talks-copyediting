% The Basics of Navigating FLOSS Licenses
% Bradley M. Kuhn
% Thursday 28 March 2013

# Who Am I?

+ President and Executive Director of Software Freedom Conservancy.

+ A 501(c)(3) not-for-profit charity dedicated to promoting, advancing, defending, and developing Free, Libre and Open Source Software.

+ I used to be software developer.

+ I was Executive Director (2001-2005) of the Free Software Foundation (creator of GPL)
     + I'm still on FSF's Board of Directors as a volunteer. 

# Ye Ol' Four Freedoms

+ to use.

+ to learn and modify for yourself.

+ to copy and share.

+ to modify and share modified versions.

# Why Do We Need Licenses?

+ None of those freedoms happen by default.

+ Software is governed by copyright &hellip;
       + &hellip; since it's expression fixed in a tangible medium.

+ only way to give those freedoms is to give a license.

# What Free Software License Does?

+ Copyright on software governs three basic activities:
      + copying, modifying and distributing.

+ By default, you're prohibited from doing those things.

+ A license is:
      + a document that gives you permissions &hellip;
      + &hellip; that you otherwise wouldn't have.

+ Free software license, first and foremost:
      + assert recipient's to copy, share, modify and redistribute the software.

# Does/Should a License Do More?

+ Ultimately depends on the authors' goal.
      + More on this in a moment.

+ Every Free Software license gives first recipient those four freedoms.

+ Question is: What about everyone else?

# Basics of Copyleft Licensing

+ Concept: share and share alike.

+ Accomplished via copyright requirements:
      + distribution accompanied with complete, corresponding source.
      + distribution of modified versions need same (with changes).

+ Generally triggered on distribution:
      + (Note: Affero GPL triggers on modification.)

# Comparing Licenses

<br/>
<br/>

<img src="license-spectrum.png" align="center" />

# Contribution Models

+ Both permissive and copyleft communities **want** changes back.

+ The Free Software Community is separated into two equally important groups:
     + The permissively licensed, who use social pressure to liberate code.
     + The copyleft licensed, whose license requires liberation.

# On Requirement By License

<img src="Constitution.jpg" align="right"  />

+ Social pressure *does* work &hellip;
     + &hellip; and should be used (and is), even for copylefted software.
     + &hellip; license enforcement is a last resort.

+ GPL == Constitution of Software Freedom Land.
     + a &ldquo;written down&rdquo; embodiment of core principles.

+ GPL's a detailed implementation of the four freedoms:
     + freedom to run and study.
     + freedom to improve.
     + freedom to share.
     + freedom to share improvements.

# The "More Free" Argument

+ Permissive license communities argue they give &ldquo;more freedom&rdquo;
     + This is a ultimately a Libertarian argument.
     + Businesses are &ldquo;free&rdquo; to do whatever they want.
     + Dispute is not unlike &ldquo;role of government&rdquo; debates.

+ Such debates are not ultimately helpful.

# How Do you Select a License?

+ Use one that is both on the <a href="http://www.gnu.org/licenses/license-list.html">FSF's</a> and <a href="http://opensource.org/licenses">OSI's</a> lists.

+ Please don't try to write your own.
      + Particularly if corporate attorneys are involved.

+ If you're giving to a contribution to a project, use the license already selected by the project.
 
# More Info / Talk License

+ URLs / Social Networking / Email:
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: faif.us, ebb.org/bkuhn &amp; @bkuhn (identi.ca only)
     - Slides at: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)
     - Please donate: sfconservancy.org/donate/

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2010, 2011, 2012, 2013 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>

<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
