% Software Freedom Conservancy: Non-Profit Infrastructure For FLOSS
% Bradley M. Kuhn
% Thursday 27 October 2011

# Why *Did* People Write Free Software?

<img src="gnu-head.jpg" align="right" />

+ Consider GNU project.

+ Founded on idealism.

+ Recruited volunteers.

+ Paid non-profit staff.

+ Those projects remain community-oriented.

# The Danger of Our Success

+ Open Source is a corporate *fad* now.

+ VC's use Open Source manipulatively
     - proprietary relicensing
     - proprietary add-ons

+ Companies employ developers &hellip;
     - &hellip; maybe to work on Free Software.
     - &hellip; but companies control employee's time in their interest.

# Where Things Are Heading

+ FLOSS slowly goes toward for-profit corporate interest.

+ Or, it's purely thrown over the wall.
    - At corporate whim
    - cf: Android delayed release

+ Project decisions are made by companies.

# Software Freedom Governance?

+ Corporate sponsorship are common.

+ For-profit control of projects can be dangerous.

+ Fedora vs. Ubuntu
    - loss leader products to upsell RHEL, etc.

# Danger of For-Profits

+ For-profits act in interest in shareholders.

+ Those who care most about software are usually *not* shareholders.

+ Interests only align sometimes with community.

# Non-Profits are Different

+ USA 501(c)(3) non-profits act in interest of the public good.
    - similar to: Stichting in Netherlands or 1901 non-profit association in France

+ Software freedom is best when in public good.

+ Non-profit orgs (NPOs) are best place for software freedom.

# What Can USA NPOs Do?

+ Collect (USA-tax-deductible) donations for a project.
      - both individual and corporate.

+ Distribute that money to advance project (and public good).

+ Make sure project isn't controlled by for-profit interests.

+ Help leadership with non-technical decisions.

+ Neutral ground when corporations sponsor.
 
# Organization Proliferation

+ Too many NPOs will confuse donors.

+ Need a board of directors, incorporation, meetings, IRS filings.

+ Hackers end up doing work they hate.

+ Less Free Software gets written.

# Aggregating Under Umbrella

+ Conservancy is designed as one organization.

+ Composed of many different projects.

+ Each has its own funds.

+ One board of directors, one non-technical governance structure.

# Funding Through Conservancy 

+ Travel to conferences.

+ Handling Google SoC and other philanthropy programs.

+ Conference invoicing and contracts.

+ Stipends and paid development contracts for developers.

+ Fiscal oversight.

# How to Join?

+ Looking for established projects.

+ Relatively informal process.

+ Email info@sfconservancy.org

+ 6-8 month signup lead time.

# More Info and This Talk's License

+ URLs / Social Networking / Email:
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: ebb.org/bkuhn &amp; @bkuhn (identi.ca only)
     - FSF Licensing Site: fsf.org/licensing
     - Report GPL violations: &lt;compliance@sfconservancy.org&gt;
     - Slides at: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)

Copyright &copy; 2010, 2011 Bradley M. Kuhn.
<a rel="license"
href="http://creativecommons.org/licenses/by-sa/3.0/us/"><img
alt="Creative Commons License" style="border-width:0" src="cc-by-sa-3-0_88x31.png"
/></a><br />These slides, this talk, and audio/video recordings thereof
are licensed under the <a rel="license"
href="http://creativecommons.org/licenses/by-sa/3.0/us">Creative Commons
Attribution-Share Alike 3.0 United States License</a>.
