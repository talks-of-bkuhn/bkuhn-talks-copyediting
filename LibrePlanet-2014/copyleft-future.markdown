% Considering the Future of Copyleft
% Bradley M. Kuhn
% Sunday 2 February 2014

# Talking Politics and Faith

+ My mother told me:
     + never discuss religion or politics in polite company.
     + but I was quite a rebellious kid.

+ Fact is, anything worthwhile is mired in politics.
     + because reasonable, rational actors simply disagree on politics.

+ Open Source and Free Software is a trecherous political system.
     + &hellip; not often noticed because nearly all our politics are played by proxy.  

# A Recitation of Faith

+ I believe everyone should have the right to copy, share, modify and redistribute all software.
      + i.e., I believe in universal software freedom.

+ I believe that a license can and should be used to defend software freedom (to the maximum extent allowed by law).
      + i.e., I believe in strong copyleft.

+ I believe each developer has the right to decide what Free Software license they choose.

+ These are moral beliefs, admittedly not facts of truth.
      + but the societal permission to make software proprietary is a moral belief on the other side.

# Should Copyleft Exist?

+ I'm not objecting to the debate: Should we have copyleft?

+ But I'm somewhat sick of defending the idea of copyleft.

# The Truce We Once Had

<img src="mundie-behlendorf.jpg" align="right"  />

+ OSCON 2001.

+ Microsoft &ldquo;Shared Source&rdquo; anti-copyleft campaign.

+ Brian Behlendorf of Apache Software Foundation stood up for copyleft.

+ Would ASF do that today?


# Back When It Was a Cause &hellip;

+ In high school, I got beat up for being in the environmental club.

+ We put old carboard boxes in the classrooms, and collected them every Friday.

+ I doubt there are many schools that don't recycle their paper now.
      + on the surface, that's progress.

# The Politics of Cooption
<img src="green-desk.png" align="right"  />

+ I just moved out of a cow-orking facility, called Green Desk.
       + marketing: cow-orking that's good for the environment.

+ They didn't even recycle the paper there: just bins to pretend.
      + It's not the first office I've worked in like this.
      + Usually, I'm the only one who cares.
      + most say: eh, &ldquo;what are you going to do?&rdquo;

# The Politics of Cooption

+ Once an idea becomes accepted as the &ldquo;right thing to do&rdquo;
      + &hellip; lip service works better than getting it right.

+ Take the exploitable parts and leaving the rest.
      + Ultimately, this is what Open Source is.   

# Non-Copyleft Is Easier

+ If your priority is revenue, proprietary software is a better option.
      + or, at least, keep your options open: it's just good business.

+ Usual Open Source arguments still apply:
      + upstream code when it behooves you &hellip;
      + &hellip; don't when it doesn't.

# OSCON, 12 years later

<img src="preston-werner.jpg" align="right"  />

+ Tom Preston-Werner
     + yes, the &ldquo;open source almost everything guy&rdquo;

+ Claims at OSCON 2013:
     * &ldquo;The GPL is a license of restrictions; I don't like restrictions. just
       use MIT&rdquo; [sic]
     * This is so transparently self-serving, yet it's widely accepted.

+ Meanwhile, we gotta (sometimes) play same &ldquo;dirty&rdquo; politics they do.
     + If Preston-Warner is discredited for other reasons, we should pounce.
     + I don't acting like a politician, but we won't win if we don't.
     
# Apache Software Foundation

+ If there going to say this &hellip;
    + &hellip; **why** are we being nice to them politically?

<hr/>

<span class="fitonslide">

<p align="left">
This is not just a theoretical concern. As aggressively as the BSA protects
the interests of its commercial members, [GPL enforcers] protects the GPL
license in high-profile lawsuits against large corporations. &hellip; FSF
&hellip; writes about their expansion of "active license enforcement.  So the
cost of compliance with copyleft code can be even greater than the use of
proprietary software, since an organization risks being forced to make the
source code for their proprietary product public and available for anyone to
use, free of charge.
</p>
<p align="left">
<center>
The Apache Advantage
</center>
</p>

<p align="left">
However, not all open source licenses are copyleft license [sic]. Not all of them
have that viral quality that radically increases the risk for an
organization. A subset of open source licenses, generally called "permissive"
licenses, are much more friendly for corporate use.
</p>

<p align="right">
 &mdash; <a
 href="https://www.openoffice.org/why/why_compliance.html">Apache
 Software Foundation, *Compliance Costs and the Apache License*</a>
</p>
</span>

# Divide And Conquer

+ Microsoft wasn't powerful enough to do what a thousand uncoordinated start-ups can.

+ This isn't a conspiracy; it's a sponteneous alignment of independant self-interest.

# Those Who Forget the Past&hellip;

+ Why is GNU/Linux preferred overwhealming for servers over the BSDs?
      + BSDs have great code:
      + a few infrastructure pieces are essential: TCP/IP, ssh

+ Apple sends *just enough* back to BSDs in resources to keep the core they need.
      + adoption vs. software freedom argument, all over again.

+ This will happen with LLVM too.
      + &hellip; but GCC **will** lose a lot of adoption before the pain begins.

# This Game Is Over

+ Free Software played to stalemate with proprietary on the operating system.
      + We got everything from kernel to application API inside the VM servers.
      + They got everything else.

+ Fourtuately, the software got better overall.
      + &hellip; but that's not nearly enough for universal software freedom.

# The Two New Games

+ application delivery to the browser. 

+ the &ldquo;embedded&rdquo; device, including mobile here.

# The Blurry Line of Javascript

+ Instantaneous install of applications that looks like a page hit.

+ The blurring line of source to object code.
      + so subtle that few see it.
      + Of course, RMS saw this way back when he wrote GPLv2: &ldquo;preferred form of the work for making modifications to it&rdquo;.

+ &hellip; but many who do perceive it differently.

# So, raise your hands:

+ How many of you run LibreJS and/or NoScript browser plugins?

+ How many of use Chromium?

# xdm, Solaris and NIS+

+ I once chased a bug from xdm, down through NIS+ to the Solaris kernel

+ Sun told me my company was &ldquo;too small&rquo; to get it fixed.

+ It's one of the experienced that turned me into a Free Software zealot.

+ Who does this happen to anymore?
    + (almost) no one!
    + Why?

# Chasing the Bug Down the Stack

+ Meanwhile: even proprietary software got better.

+ All software got more complex.

+ The layers of proprietarization got thinner.

# Today's Developers: Children of the Web

+ You never have all the source &hellip;

+ &hellip; but you're often fed a JSON API.

+ Javascript developers don't consider idea they'd have &ldquo;all the source&rdquo; on hand.

+ &amp; they didn't have a copyleft license anyway ...
      + b/c GPL is the ISC license of web applications.

+ These are also people who found the startups that hate copyleft.
      + Their employers don't know they need copyleft.
      + and these employers let you upstream a bit, after all.

# As the Web grows up

+ They'll reinvent the wheel more.
     + Like all of us old Unix hackers did.

+ Once you do that enough, you pine for copyleft.

+ Cooption will hopefully stop working.
     + &hellip; but that may take a generation.

+ But that may take a generation, meanwhile &hellip;

# Reboostrapping

+ Slowly, the cooption leaves the only essential GPL'd program as Linux.

+ Copyleft works only when we have code that companies can't live without.
      + And if GPL isn't treated like GPL, it doesn't work either.

+ The cooption pushes from this side too.

# Logically Consistent Copyleft?

<img src="wonderful-life_george-clarence-wisper-at-bar.jpg" align="right"  />

+ **GEORGE:** Look, uh &hellip; I think maybe you better not mention getting your wings around here.
+ **CLARENCE:** Why? Don't they believe in angels?
+ **GEORGE** I&hellip; yeah, they believe in them&hellip;
+ **CLARENCE:** Ohhh &hellip; Why should they be surprised when they see one?

# Logically Consistent Copyleft?

+ It's fundamentally hypocrisy to say you support copyleft but oppose GPL enforcement.  
     + Yet, this is the **most common position by for-profit companies**.

# If It's Just a Symbol

+ If GPL is just merely a symbol with no real authority.
     + why bother?
     + We might as well all be using the Apache license.  

+ An unenforced copyleft is the functional equivalent to a permissive license.

<hr/>

> Well, if it’s a symbol, to hell with it.

<span class="fitonslide">
<p align=right>
 &mdash; <a href="http://frdenis.blogspot.com/2013/06/if-its-symbol-to-hell-with-it_2732.html">Flannery O’Connor, **The Habit of Being**</a>
</p>
</span>

# Enforcement, then start hacking

+ We'll have to reestablished the copyleft of Linux and its value.
      + Enforcement is the only way to do it.

+ We'll need to **code**, **code**, **code** useful copylefted stuff.

+ Old school copyleft hackers: go learn Node.js.
      + or whatever the kids are into.
      + code on your own time.
      + And tell your employer you want to keep your copyrights, &
      + copyleft your stuff: with Affero GPL.

# More Info / Talk License

+ URLs / Social Networking / Email:
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: faif.us, ebb.org/bkuhn &amp; @bkuhn (identi.ca)
     - GPL violations: &lt;compliance@sfconservancy.org&gt;
     - Slides: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)
     - DONATE: http://sfconservancy.org/donate/

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2008, 2009, 2010, 2011, 2012, 2013 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>

<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
