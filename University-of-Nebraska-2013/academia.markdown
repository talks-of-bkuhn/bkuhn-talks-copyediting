% Interactions Between FLOSS NGOs, the Software Industry and Academia
% Bradley M. Kuhn
% Monday 11 February 2013

# My History

+ In a CS PhD program at University of Cincinnati (1997-2000).

+ Dropped out with a Master's to do policy work for Free Software Foundation.

+ Worked at various non-profits.

+ Now I run a 501(c)(3) fiscal sponsor non-profit, called Software Freedom Conservancy.

+ I'm not a researcher; I'm a policy wonk &amp; advocate for software freedom.

# In The Beginning ...

<img align=right src="pdp-10.jpg" />

+ There was a computer.

+ There were terminals.

+ There were users.

+ ... and the users had freedom.

# Ye Ol' Four Freedoms

+ to use.

+ to learn and modify for yourself.

+ to copy and share.

+ to modify and share modified versions.

# The Golden Age of Computing

<img src="pdp-10-2.jpg" align="right"  />

 - academic computing.
 - software sharing.
 - no licenses!

# BSD Project

+ 1974: AT&amp;T gives University of California Berkeley full Unix source.

+ BSD releases its own distribution.

+ No one worries about the licensing, until &hellip;


# &hellip; Then Freedom Eluded Us

+ They built licensing walls.

+ By separating the software from the computer.

<img align=right src="berlin-wall-build.jpg" />
</center>

+ Who Invented Licensing?

# Who Invented Licensing?
<br/>
Some think he did:
<br/>
<img src="bill-gates-arrest.jpg" align="left"  />
<p>As the majority of hobbyists must be aware, most of you steal your software. 
&hellip; [If] software is something to share &hellip; You prevent good software from being written.</p>

<br clear="all"/>

+ Gates convinces IBM to license rather than acquire DOS.

# BSD Lawsuit

+ AT&amp;T monopoly gone: 1987 break-up.

+ Unix copyrights end up with USL.

+ They sue UC-Berkeley in 1992.

+ Unresolved until mid 1990s
    - Legal BSD release left incomplete system.
    - Took years for BSD to work again.

+ BSD, as a project, only barely recovered.

# The Rise of Copyleft

<center>
<br/>
<br/>
<img src="rms-current.jpg"  />
</center>

# GNU Project

<img src="gnu-hacker.jpg" align="right" />

+ RMS founds GNU project around copyleft licensing.
      - use copyright law against itself to uphold freedom. 

+ Founded on idealism.

+ Recruited volunteers.

+ Paid non-profit staff.

+ Centered around MIT.

# MIT AI Lab

<img src="ai-lab-tear-down.jpg" align="right"  />

But MIT was changing.

# MIT AI Lab

<cite>patents == $$$</cite>

# MIT AI Lab

<cite>spin-offs == $$$</cite>

# Academic Patent Licensing

+ MIT gets USA Patent 4,405,829 on RSA.

+ USA is first to invent (until 2011).

+ MIT launches spin-off: RSA Data Security.

+ Sets back adoption of public key cryptography by a decade (IMO).

# The Cycle

+ Government funds research.

+ Universities gain patents &amp; copyrights.

+ Universities launch spin-offs.

+ Universities get money.  

# Unintended consequences?

<img src="shuttleworth-spacesuit.jpg" align="right"  />

+ Ever wonder why he's so wealthy?

# Selling Patented Tech From Outside USA

+ RSA was published in 1977; patented years later.

+ Few places are "first to invent".
       + Patent applies only in a few countries.

+ Shuttleworth sells RSA certificates back to USA cheaply.
       + Undercuts Network Solutions.

+ Why'd we let that wealth leave USA for South Africa? 

# My Own Master's Thesis

+ University of Cincinnati Copyright Policy says:
       + Graduate students hold copyright on their thesis document.
       + But not on their software!
       + Don't even hold copyright on your own **homework**.

+ Hired a lawyer, but couldn't get University lawyers to budge.

+ So, I put my source code as an appendix to my thesis document &hellip;
    - &hellip; and released it as Free Software.

# How Collaboration Should Work

+ Academic software systems should be released in software freedom.

+ The four freedoms are **designed** for academic exploration.

+ Yet, universities function like businesses.

+ We should be agents of change on this issue.

# The Irony of Business Research

<img src="lab-rat.jpg" align="right"  />

+ FLOSS community has become a favorite research target.
    - Business schools
    - Anthropologists
    - Sociologists

+ I've been a lab rat in four different studies.

+ No real objection, but I'd rather researchers would &ldquo;go native&rdquo;.

# Research from the CS-side

+ So many research systems are released as proprietary software.

+ The referred paper is king: what about the software?

+ Even systems without commercial viability are kept proprietary.

+ NSF funding shouldn't go toward proprietary software.
    - that's the &ldquo;people's code&rdquo;.

# Technology Transfer

<img src="CACM-cover-1996-09.jpg" align="right"  />

+ Perennial issue for academic research.

+ Corporate spin-offs aren't **charitable** technology transfer.
      + Does it fulfill University's 501(c)(3) mission?
      + IRS legality is an &ldquo;open question&rdquo;. [Milton2010]

+ Obama's Jobs Council says:
      - &ldquo;the Administration should test an &lsquo;open-source&rsquo; approach to tech transfer and commercialization.&rdquo; [JobsCouncil2011]

+ Academia should take lead here.

# Haskell

+ Haskell wasn't merely an academic language:

+ University of Glasgow released implementation as Free Software.

+ Many contributors; both volunteer &amp; for-profit industry interest.

+ Now widely used Free Software programming language.

+ Reference: [Hudak2007]

# What Your Students Need

+ Linux-based systems are everywhere (Android, etc.).

+ So many other FLOSS systems are standard.

+ Your students need to know how to contribute.

+ FLOSS contribution should be a standard course:
      + Just like Software Engineering or Algorithms.

# How NGOs Can Help?

+ FLOSS communities are notoriously difficult to engage with.

+ Purely industry-centered &ldquo;communities&rdquo; are common.
      + But designed as a trade associations.

+ What's the space in-between academia &amp; industry in FLOSS:
      + The FLOSS 501(c)(3) non-profit organizations.
      + (Same tax status as Universities).

# What's Conservancy Do?

+ Project communities join.

+ Become part of the org (as if they'd incorporated a non-profit).

+ Conservancy handles grants, donations, legal issues.
       + anything other than coding that needs done.

+ Becomes a center-point for collaboration &amp; communication with project.


# References

<span class="fitonslide">
<p>[Hudak2007] Hudak, et al.  <a href="http://research.microsoft.com/en-us/um/people/simonpj/papers/history-of-haskell/history.pdf">&ldquo;A History of Haskell:Being Lazy With Class&rdquo;</a>.  Proceedings of the third ACM SIGPLAN conference on History of programming languages". Pages 12-1-12-55. (<a href="http://research.microsoft.com/en-us/um/people/simonpj/papers/history-of-haskell/history.mp4">Video of talk</a>)</p>
<p>[Milton2010] Cerny, Milton and Hellmuth, Kelly. <a href="http://www.mcguirewoods.com/news-resources/publications/taxation/TaxationofExempts-May-June2010.pdf">&ldquo;Economic Crises? Technology Transfer to the Rescue&rdquo;</a>. 21 Taxation Exempts 6 (2010).</p>

<p>[JobsCouncil2011] President's Council on Jobs and Competitiveness. <a href="http://files.jobs-council.com/jobscouncil/files/2011/10/JobsCouncil_InterimReport_Oct11.pdf">&ldquo;Taking action, building confidence:  Five Common-Sense Initiatives to Boost Jobs and Competitiveness&rdquo; Interim Report</a>. October 2011.</p>
</span>

# More Info / Talk License

+ URLs / Social Networking / Email:
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: faif.us, ebb.org/bkuhn &amp; @bkuhn (identi.ca only)
     - Slides at: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2008, 2009, 2010, 2011, 2012, 2013 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>

<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
