% Non-Profit Centers for FLOSS Development
% Bradley M. Kuhn
% Sunday 15 September 2013

# My History

+ In a CS PhD program at University of Cincinnati (1997-2000).

+ Dropped out with a Master's to do policy work for Free Software Foundation (on its Board these days).

+ Worked at various non-profits.

+ Now I run a 501(c)(3) fiscal sponsor non-profit, called Software Freedom Conservancy.

+ I'm not a researcher; I'm a policy wonk &amp; software freedom advocate.
     + No, I didn't do a study to come to my policy conclusions.

# A Brief Non-Profit Free Software History

<img src="rms-80s-scaled.png" align="right" />

+ 1984-09-27: GNU Manifesto.

+ RMS wrote in that *GNU Manifesto*:
      - *"All sorts of development can be funded with a Software Tax"*
      - *"People with new ideas could distribute programs as [free software], asking for donations from satisfied users"*

+ A non-profit model for the community was planned from the start.

# Some FSF History

<img src="gnu-head.jpg" align="right" />

+ FSF is founded in 1984.

+ Gets 501(c)(3) status in 1985.

+ FSF's first 12 years is mostly to employ developers!
      - 46% of FSF's history.
      - Some of the key developers who now lead many of the largest Free Software projects.
      - (Source: GNU's Bulletins 1986-1997)
      

# The Rise of the Volunteers

+ Most Free Software is written by volunteers &hellip;
     + &hellip; who are only such from the point of view of the projects.

+ The volunteers are usually paid for by for-profits
     - Red Hat, Google, IBM, etc.

# The Rise of &ldquo;Open Source&rdquo;

+ Open Source, that thing most of you study, is just a business phenomena.

+ It's a concept specifically designed as an amoral alternative to software freedom politics.
     - [see Evgeny Morozov's article, *The Meme Hustler*](http://thebaffler.com/past/the_meme_hustler)

+ For-profit companies always prefer the amoral.

+ I'd love to see the areas of academics that relate most to software freedom engaged:
     - Political Science
     - Philosophy

# For-Profit vs. Non-Profit

+ For-profits act in interest in shareholders.

+ 501(c)(3) non-profits act in interest of the public good.

+ Software freedom is best when in public good.

+ Non-profit orgs (NPOs) are best place for software freedom.

+ NPOs can accept for-profit donations, but provide a firewall.

# For-Profit-Employee &ldquo;Volunteers&rdquo;

+ Most code written by for-profit employees.
     + *20% time* is a boon to software freedom.
     + **But** codebases drift in directions of company's needs.

+ Some developers should be *funded* by non-profits,
     - to mitigate dangers of for-profit control.

<hr/>

> It's the duty of all Free Software developers to steal as much time as they can from their employers for software freedom.

<span class="fitonslide">
<p align=right>
 &mdash; Jeremy Allison, Director, Conservancy &amp; Co-Founder, Samba Project
</p>
</span>

# What Can NPOs Do?

+ Collect (USA-tax-deductible) donations for a project.
      - both individual and corporate.

+ Distribute that money to advance project (and public good).

+ Make sure project isn't controlled by for-profit interests.

+ Help leadership with non-technical decisions.

# Why Do We Need Non-Profits?

+ What's wrong with for-profits?
      - that duty to shareholders gets in the way.
      - at best, business interest merely accelerates pace.
      - at worst, it distorts the community into trade-association thinking.

+ Only non-profits can view community, sharing, helping, and learning as paramount.

+ True **control** by for-profit companies **will** kill the gravy train.
      - No, I can't prove that with data.
      - But &#x2203; plenty of anecdotes &amp; &hellip;
      - &hellip; my 20 years of intuition, FWIW.

# Quick Analogy

<img src="alfie-winnie.png" align="right" />

+ Which, I admit, is perhaps a pathetically thin pretext to put a photo of my dogs in this presentation.

# The Pet Industry

<img src="alfie.png" align="right" />

+ For-profit companies abound &hellip;
      - pet food, veterinarian clinics, pet stores.

+ Some have non-profit equivalents, but why?
      - pet stores historically biggest puppy mill purchasers.
      - non-profit shelters &amp; rescues provide **morally** centered method for pet acquisition.

<img src="winnie.png" align="right" />

+ For-profit companies adapt:
      - donate pet food to shelters.
      - host pet adoption events at pet stores.
 
+ Puppy mills would be norm w/out NPOs.
     - &amp; proprietary software exploitation'd be norm w/out healthy Free Software NPOs.


# A Very Few Words On Licensing

+ My actual GPL talk is Tuesday at 17:10 in Celestin C.

+ But, given I've done more GPL enforcement than anyone on the planet &hellip;
      - I should probably say at least one thing about GPL compliance, which is:

+ License compliance is trivial if you make nothing proprietary &amp; do all you work upstream.
      - &hellip; which is the ethical &amp; moral thing to do, anyway.
      - &hellip; so just do that &amp; never worry about compliance problems again.

# More Info / Talk License

+ URLs / Social Networking / Email:
     - Conservancy: sfconservancy.org &amp; @conservancy
     - Me: faif.us, ebb.org/bkuhn &amp; @bkuhn (pump.io clients only)
     - Slides at: ebb.org/bkuhn/talks &amp; gitorious.org/bkuhn/talks (source)

+ Please donate: sfconservancy.org/donate/ or Conservancy's projects' sites.

<span class="fitonslide">
<p>Presentation and slides are: Copyright &copy; 2010, 2011, 2012, 2013 Bradley M. Kuhn, and are licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/usa/">Creative Commons Attribution-Share Alike (CC-By-SA) 3.0 United States License</a>. <img src="cc-by-sa-3-0_88x31.png"/></p>

<p>Some images included herein are &copy;'ed by others. I believe my use of those images is fair use under USA &copy; law.  However, I suggest you remove such images if you redistribute these slides under CC-By-SA-USA 3.0.
</p>
</span>
